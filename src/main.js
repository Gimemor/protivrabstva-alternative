import '@babel/polyfill'
import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import VueI18n from 'vue-i18n'
import i18n from './i18n'
import VueClipboard from 'vue-clipboard2'
import BackToTop from 'vue-backtotop'
import Vuex from 'vuex'
import store from './store'
import axios from 'axios';

Vue.use(Vuex)
Vue.use(BackToTop)
Vue.use(VueI18n)
Vue.use(VueClipboard)

axios.defaults.baseURL = 'https://t-meton.ru/slavery/Controler/'

Vue.config.productionTip = false

new Vue({
  router,
  i18n,
  store,
  render: h => h(App)
}).$mount('#app')
