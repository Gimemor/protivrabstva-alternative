import axios from 'axios'
import store from '../store.js'
import router from '../router'

export default class ApiAuthService {
  static checkToken () {
    let token = store.getters.getToken;
    return !!token
  }

  static login (username, password) {
    const params = new URLSearchParams();
    params.append('login', username);
    params.append('pass', password);
    return axios.post(
      'ControlerLogin.php',
      params
    )
  }

  static authRequest (username, password) {
    store.dispatch('AUTH_REQUEST', { username, password }).then(resp => {
      router.push('/admin')
    })
  }

  static logout () {
    store.dispatch('AUTH_LOGOUT')
      .then(() => {
        router.push('/login')
      })
  }
}
